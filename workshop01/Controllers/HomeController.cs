using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using workshop01.Models;

namespace workshop01.Controllers;

public class HomeController : Controller
{

    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        string[] x = new string[5] { "Logic will get you from A to B.  Imagination will take you everywhere.", "There are 10 kinds of people. Those who know binary and those who don't.", "There are two ways of constructing a software design. One way is to make it so simple that there are obviously no deficiencies and the other is to make it so complicated that there are no obvious deficiencies.", "It's not that I'm so smart, it's just that I stay with problems longer.", "It is pitch dark. You are likely to be eaten by a grue." };
        Random r = new Random();


        int rInt = r.Next(0, 5);


        while (rInt.Equals(TempData["selected"]))
        {
            rInt = r.Next(0, 5);
        }


        ViewData["Message"] = x[rInt];

        TempData["selected"] = rInt;

        return View();

    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
